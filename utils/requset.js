export default (params)=>{
	uni.showLoading({
		title:"加载中"
	})
	return new Promise((reslve,reject)=>{
		uni.request({
			url:"http://192.168.1.84:8081"
			...params,
			success(res) {
				reslve(res.data[1])
			},
			fail(err) {
				reject(err)
			},
			complete(){
				uni.hideLoading();
			}
		})
	})
}