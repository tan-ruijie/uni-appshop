let httpUrl = "http://192.168.1.84:8081/m.api"
// 不带token头
let httpRequest = (opts, data) => {
	let httpDefaultOpts = {
		url: httpUrl,
		data: data,
		method: opts.method,
		header: opts.method == 'get' ? {
			'X-Requested-With': 'XMLHttpRequest',
			"Accept": "application/json",
			"Content-Type": "application/json; charset=UTF-8"
		} : {
			'X-Requested-With': 'XMLHttpRequest',
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
		dataType: 'json',
	}
	let promise = new Promise(function(resolve, reject) {
		uni.request(httpDefaultOpts).then(
			(res) => {
				resolve(res[1].data)
			}
		).catch(
			(response) => {
				reject(response)
			}
		)
	})
	return promise
}


//带Token请求
let httpTokenRequest = (opts, data) => {
	let token = uni.getStorageSync("accessToken");
	//此token是登录成功后后台返回保存在storage中的

	let httpDefaultOpts = {
		url: httpUrl,
		data: data,
		method: opts.method,
		header: opts.method == 'get' ? {
			'accessToken ': token,
			'X-Requested-With': 'XMLHttpRequest',
			"Accept": "application/json",
			"Content-Type": "application/json; charset=UTF-8"
		} : {
			'accessToken': token,
			'X-Requested-With': 'XMLHttpRequest',
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
		dataType: 'json',
	}
	let promise = new Promise(function(resolve, reject) {
		uni.request(httpDefaultOpts).then(
			(res) => {
				resolve(res[1].data)
			}
		).catch(
			(response) => {
				reject(response)
			}
		)
	})
	return promise
};

export default {
	httpUrl,
	httpRequest,
	httpTokenRequest
}
